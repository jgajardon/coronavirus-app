import React, { useState, useEffect } from 'react';
import { Card, Select, Row, Col, Spin, Tooltip, Alert, Menu } from 'antd';

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip as TooltipRechart,
  Legend,
  ResponsiveContainer
} from 'recharts';

import './Home.scss';

export default function Home() {
  const [generalData, setGeneralData] = useState([]);
  const [data, setData] = useState([]);
  const [dataFiltered, setDataFiltered] = useState([]);
  const [dataForDay, setDataForDay] = useState([]);

  const [defaultCountry, setDefaultCountry] = useState('Chile');
  const [minDate, setMinDate] = useState('');

  const [error, setError] = useState(false);

  const [countries, setCountries] = useState([]);

  useEffect(() => {
    // Create an scoped async function in the hook
    async function fetchCoronavirusAll() {
      setError(false);
      try {
        const response = await fetch(
          `http://coronavirusapi-env.eba-hsqjkxrc.us-east-1.elasticbeanstalk.com:3001/coronavirus/all`
        );
        const json = await response.json();
        setGeneralData(json);
      } catch (error) {
        setError(true);
      }
    }

    async function fetchCoronavirusCountries() {
      setError(false);
      try {
        const response = await fetch(
          `http://coronavirusapi-env.eba-hsqjkxrc.us-east-1.elasticbeanstalk.com:3001/coronavirus/countries`
        );

        const json = await response.json();
        let dataFil = json.filter(x => x.country === 'Chile');
        setData(json);
        setDataFiltered(dataFil);
      } catch (error) {
        setError(true);
      }
    }

    async function fetchCountriesCodes() {
      setError(false);
      try {
        const response = await fetch(`https://restcountries.eu/rest/v2/all`);
        const json = await response.json();
        setCountries(json);
      } catch (error) {
        setError(true);
      }
    }

    async function fetchCoronavirusForDay() {
      setError(false);
      try {
        const response = await fetch(
          `https://pomber.github.io/covid19/timeseries.json`
        );
        const json = await response.json();
        setDataForDay(json);
      } catch (error) {
        setError(true);
      }
    }
    // Execute the created function directly
    fetchCoronavirusAll();
    fetchCoronavirusCountries();
    fetchCountriesCodes();
    fetchCoronavirusForDay();
  }, []);

  if (error) {
    return 'Ha ocurrido un error al cargar los datos';
  }

  if (
    dataFiltered.length === 0 ||
    countries.length === 0 ||
    generalData === 0 ||
    dataForDay.length === 0
  ) {
    return <Spin size='large' style={{ marginLeft: '50%', marginTop: 200 }} />;
  }

  const changeCountry = country => {
    let countryFormated = country === 'S. Korea' ? 'Korea, South' : country;
    let dataFil = data.filter(x => x.country === country);
    setDataFiltered(dataFil);
    setDefaultCountry(countryFormated);
  };

  let codeCountry = countries.filter(x => x.name === dataFiltered[0].country);
  let codigo = codeCountry.length === 0 ? null : codeCountry[0].alpha2Code;

  return (
    <>
      <Row style={{ marginTop: 100 }}>
        <Col span={24}>
          <div className='site-card-wrapper'>
            <h1 style={{ textAlign: 'center' }}>
              Información Mundial del Coronavirus
            </h1>
            <Row gutter={16}>
              <Col span={8}>
                <Card title='Casos' bordered={false} className='card-label'>
                  <img
                    className='image-all'
                    src={`https://image.flaticon.com/icons/png/512/1458/1458496.png`}
                  ></img>
                  <p style={{ marginTop: 20, color: 'gray' }}>
                    <strong className='label-general-data'>
                      {generalData.cases}
                    </strong>
                  </p>
                </Card>
              </Col>
              <Col span={8}>
                <Card title='Muertes' bordered={false} className='card-label'>
                  <img
                    className='image-all'
                    src={`https://cdn3.iconfinder.com/data/icons/halloween-29/64/grave-512.png`}
                  ></img>
                  <p style={{ marginTop: 20, color: 'red' }}>
                    <strong className='label-general-data'>
                      {generalData.deaths}
                    </strong>
                  </p>
                </Card>
              </Col>
              <Col span={8}>
                <Card
                  title={<Tooltip title='Sanados'>Sanados</Tooltip>}
                  bordered={false}
                  className='card-label'
                >
                  <img
                    className='image-all'
                    src={`https://i.ya-webdesign.com/images/panda-clipart-heart-25.png`}
                  ></img>
                  <p style={{ marginTop: 20, color: 'green' }}>
                    <strong className='label-general-data'>
                      {generalData.recovered}
                    </strong>
                  </p>
                </Card>
              </Col>
            </Row>
          </div>
        </Col>
        <Col
          span={24}
          style={{ textAlign: 'center', marginTop: 20, marginBottom: 20 }}
        >
          <CustomSelect data={data} changeCountry={changeCountry} />
        </Col>
        <Col span={24}>
          {' '}
          <CardCountry
            dataFiltered={dataFiltered}
            codeCountry={codigo}
            dataForDay={dataForDay}
            defaultCountry={defaultCountry}
            minDate={minDate}
          />{' '}
        </Col>
      </Row>
    </>
  );
}

function CustomSelect(props) {
  const { data, changeCountry } = props;

  const { Option } = Select;

  function onChange(value) {
    changeCountry(value);
  }

  function onFocus() {
    console.log('focus');
  }

  if (!data) {
    return <Spin size='large' style={{ marginLeft: '50%', marginTop: 200 }} />;
  }

  return (
    <Select
      showSearch
      style={{ width: 200 }}
      placeholder='Seleccionar país'
      optionFilterProp='children'
      onChange={onChange}
      onFocus={onFocus}
    >
      {data.map(corona => (
        <Option value={corona.country} key={corona.country}>
          {' '}
          {corona.country}
        </Option>
      ))}
    </Select>
  );
}

function CardCountry(props) {
  const { dataFiltered, codeCountry, dataForDay, defaultCountry } = props;
  let marginLabel = {
    margin: 'auto'
  };
  let array = [];

  if (dataForDay[defaultCountry]) {
    dataForDay[defaultCountry].forEach(
      ({ date, confirmed, recovered, deaths }) => {
        if (confirmed > 0) {
          //console.log(date);
          let object = {
            date,
            confirmados: confirmed,
            muertes: deaths
          };
          array.push(object);
        }
      }
    );
  }

  return (
    <Card title='Información por País del Coronavirus' className='card-content'>
      <CardGrid>
        <Row>
          <Col span={12} style={marginLabel}>
            Pais <strong>{dataFiltered[0].country} </strong>
          </Col>
          <Col span={12}>
            {!codeCountry ? (
              <img
                src={`https://cdn3.iconfinder.com/data/icons/web-hosting-7/66/61-512.png`}
                style={{ width: '40px', height: '45px' }}
              ></img>
            ) : (
              <img
                src={`https://www.countryflags.io/${codeCountry}/flat/64.png`}
                style={{ width: '40px', height: '45px' }}
              ></img>
            )}
          </Col>
        </Row>
      </CardGrid>
      <CardGrid>
        <Row>
          <Col span={12} style={marginLabel}>
            Casos <strong>{dataFiltered[0].cases}</strong>
          </Col>
          <Col span={12}>
            {' '}
            <img
              src={`https://image.flaticon.com/icons/png/512/1458/1458496.png`}
              style={{ width: '40px', height: '45px' }}
            ></img>
          </Col>
        </Row>
      </CardGrid>
      <CardGrid>
        <Row>
          <Col span={12} style={marginLabel}>
            Casos hoy <strong>{dataFiltered[0].todayCases}</strong>
          </Col>
          <Col span={12}>
            {' '}
            <img
              src={`https://img.icons8.com/plasticine/2x/today.png`}
              style={{ width: '40px', height: '45px' }}
            ></img>
          </Col>
        </Row>
      </CardGrid>
      <CardGrid>
        <Row>
          <Col span={12} style={marginLabel}>
            Muertes <strong>{dataFiltered[0].deaths}</strong>
          </Col>
          <Col span={12}>
            {' '}
            <img
              src={`https://cdn3.iconfinder.com/data/icons/halloween-29/64/grave-512.png`}
              style={{ width: '40px', height: '45px' }}
            ></img>
          </Col>
        </Row>
      </CardGrid>
      <CardGrid>
        <Row>
          <Col span={12} style={marginLabel}>
            Sanados <strong>{dataFiltered[0].recovered} </strong>
          </Col>
          <Col span={12}>
            {' '}
            <img
              src={`https://i.ya-webdesign.com/images/panda-clipart-heart-25.png`}
              style={{ width: '40px', height: '45px' }}
            ></img>
          </Col>
        </Row>
      </CardGrid>
      <CardGrid marginBot={20}>
        <Row>
          <Col span={12}>
            Criticos <strong>{dataFiltered[0].critical}</strong>{' '}
          </Col>
          <Col span={12}>
            {' '}
            <img
              src={`https://cdn4.iconfinder.com/data/icons/medical-icons-normal/1000/modules_ICU.png`}
              style={{ width: '40px', height: '45px' }}
            ></img>
          </Col>
        </Row>
      </CardGrid>

      {dataForDay[defaultCountry] && (
        <>
          <div>
            <h2>Contagio por día</h2>
          </div>

          <Alert
            message='Importante'
            description='La información del gráfico, solo esta disponible hasta el dia anterior. '
            type='info'
            showIcon
          />

          <MultiLineChart data={array} />
        </>
      )}
    </Card>
  );
}

function CardGrid(props) {
  const { children, marginBot } = props;
  const gridStyle = {
    width: '50%',
    textAlign: 'center',
    marginBottom: marginBot ? marginBot : '0px'
  };
  return <Card.Grid style={gridStyle}>{children}</Card.Grid>;
}

function MultiLineChart(props) {
  const { data } = props;
  return (
    <Row>
      <Col span={24}>
        <ResponsiveContainer width='100%' height={400}>
          <LineChart
            width={1500}
            height={300}
            data={data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray='3 3' />
            <XAxis dataKey='date' />
            <YAxis />
            <TooltipRechart />
            <Legend />
            <Line
              type='monotone'
              dataKey='confirmados'
              stroke='#8884d8'
              activeDot={{ r: 8 }}
            />
            <Line type='monotone' dataKey='muertes' stroke='#82ca9d' />
          </LineChart>
        </ResponsiveContainer>
      </Col>
    </Row>
  );
}
