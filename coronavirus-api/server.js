var express = require('express');
var app = express();
var cors = require('cors');
const axios = require('axios');

app.use(cors());

app.get('/coronavirus/countries', function(req, res) {
  axios
    .get('https://coronavirus-19-api.herokuapp.com/countries')
    .then(function(response) {
      // handle success
      res.json(response.data);
    })
    .catch(function(error) {
      // handle error
      res.json({ message: `error ${error}` });
    });
});

app.get('/coronavirus/all', function(req, res) {
  axios
    .get('https://coronavirus-19-api.herokuapp.com/all')
    .then(function(response) {
      // handle success
      res.json(response.data);
    })
    .catch(function(error) {
      // handle error
      res.json({ message: `error ${error}` });
    });
});

app.get('/countries', function(req, res) {
  axios
    .get('http://country.io/names.json')
    .then(function(response) {
      // handle success
      res.json(response.data);
    })
    .catch(function(error) {
      // handle error
      res.json({ message: `error ${error}` });
    });
});

app.listen(3001, function() {
  console.log('Example app listening on port 3000!');
});
