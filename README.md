# Coronavirus App

Aplicación que muestra información mundial y por país acerca del coronavirus.

## Instalación

Para instalar la api en node, entrar a la carpeta /coronavirus-api y ejecutar el siguiente comando:

```bash
npm install
```

Para ejecutar la API de manera local, ejecutar el siguiente comando:

```bash
node server.js
```

Para instalar el front-end en React ingresar a la carpeta /coronavirus-web y ejecutar el siguiente comando:

```bash
yarn
```

Para ejecutar el front-end de manera local, ejecutar el siguiente comando:

```bash
yarn start
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
